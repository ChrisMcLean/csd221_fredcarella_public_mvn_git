/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package w20_lecture3_ex1;

/**
 *
 * @author student
 */
public class Person {
    private String firstname;
    private String lastname;

    public Person() {
        System.out.println("calling Person() constructor");
        firstname="<default first name>";
        lastname="<default last name>";
    }

    public Person(String firstname, String lastname) {
        System.out.println("calling Person(String firstname, String lastname) constructor");
        this.firstname = firstname;
        this.lastname = lastname;
    }

    /**
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @param firstname the firstname to set
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * @return the lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @param lastname the lastname to set
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @Override
    public String toString() {
        return "["+firstname+" "+lastname+"]";
    }
    
    
    
    
}
